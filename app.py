import pickle
import numpy as np

import flask
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/", methods=["post", "get"])
def main():
    if flask.request.method == 'GET':
        return render_template('main.html')

    if request.method == "POST":
        with open('lr_model.pkl', 'rb') as f:
            loaded_model = pickle.load(f)

        var1 = float(flask.request.form['var1'])
        var2 = float(flask.request.form['var2'])
        var3 = float(flask.request.form['var3'])
        var4 = float(flask.request.form['var4'])
        var5 = float(flask.request.form['var5'])
        var6 = float(flask.request.form['var6'])
        var7 = float(flask.request.form['var7'])
        X = np.array([var1, var2, var3, var4, var5, var6, var7]).reshape(1,7)
        y_pred = loaded_model.predict(X)

        return render_template("main.html", result = y_pred)
if __name__ == '__main__':
    app.run()